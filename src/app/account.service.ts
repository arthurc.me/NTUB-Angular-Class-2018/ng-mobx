import { Injectable } from '@angular/core';
import { observable, computed } from 'mobx-angular';


@Injectable()
export class AccountService {
  @observable transactions: number[] = [];
  constructor() { }

  @computed get balance(): number {
    console.count('Call Service')
    return this.transactions.reduce((a, b) => a + b, 0);
  }

  @computed get isNegative(): boolean {
    return this.balance < 0;
  }

  deposit(money: number) {
    this.transactions = [...this.transactions, money];
  }

  withdraw(money: number) {
    this.transactions = [...this.transactions, -money];
  }
}
